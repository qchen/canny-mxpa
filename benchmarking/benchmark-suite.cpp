// Benchmarks all of the image processors by timing them on test images.
// For each image, each image processor will execute canny multiple times
// to find the mean and standard deviation. Next, each stage is executed
// in isolation to determine the runtime of each stage.
#include <iostream>
#include <vector>
#include <memory>
#include "benchmark.h"
#include "openclimageprocessor.h"
#include "serialimageprocessor.h"
#include "cvimageprocessor.h"

using namespace std;
using namespace Benchmarking;
using namespace ImageProcessors;
const string IMG_PATH = "images/";

int main(int argc, char *argv[]) {
  // Images that will be used in the benchmarks as well as their resolution
  // In the future, this should get the information dynamically based on
  // the contents of the images folder.
  InputImage image = InputImage("lena.jpg", 256, 256);
  Benchmark Mxpa = Benchmark("OpenCL CPU",
        make_shared<OpenclImageProcessor>(false),
        IMG_PATH, image, 1);
  Benchmark Cpu = Benchmark("Serial",
        make_shared<SerialImageProcessor>(),
        IMG_PATH, image, 1);


  std::cout << "MxPA Run: "<< std::endl;
  Mxpa.Run();
  std::cout << "CPU Run: "<< std::endl;
  Cpu.Run();
  cv::Mat ReMxpa = Mxpa.OutputResults();
  cv::Mat ReCpu =  Cpu.OutputResults();
  std::cout << "Check the result... "<< std::endl;
  const int offset = 16;
  for (int row = 1; row < ReCpu.rows - 1; row++) {
    // iterate over the columns of the photo matrix
    for (int col = offset; col < ReCpu.cols - offset; col++) {
      const size_t cnt = row * ReCpu.cols + col;
      if (ReMxpa.data[cnt] != ReCpu.data[cnt]) {
        std::cerr << "Error: " << row << " , " <<  col <<" got("
          << (unsigned int)ReMxpa.data[cnt] << ") vs except("
          << (unsigned int)ReCpu.data[cnt] << ") " << std::endl;
        std::cout << "Failed !"<< std::endl;
        return -1;
      }
    }
  }

  std::cout << "Passed !"<< std::endl;

  cv::Mat combine;
  cv::hconcat(Mxpa.GetInputMat(), ReMxpa, combine);
  cv::imshow("Canny", combine);
  cvWaitKey(0);

  return 0;
}
