// Hysteresis Threshold Kernel
// data: image input data with each pixel taking up 1 byte (8Bit 1Channel)
// out: image output data (8B1C)
__kernel void hyst_kernel(__global uchar16 *data,
                          __global uchar16 *out,
                          unsigned rows,
                          unsigned cols) {
  // Establish our high and low thresholds as floats
  uchar16 lowThresh = (uchar16)(30);
  uchar16 highThresh = (uchar16)(70);
  uchar16 median = (lowThresh + highThresh) / (uchar)2;

  // These variables are offset by one to avoid seg. fault errors
  // As such, this kernel ignores the outside ring of pixels
  unsigned row = get_global_id(0);
  unsigned col = get_global_id(1);
  cols = cols / 16;
  unsigned pos = col * cols + row;
  uchar16 EDGE = (uchar16)(255);
  uchar16 ZERO = (uchar16)(0);
  out[pos] = (data[pos] >= highThresh) ? EDGE : (data[pos] <= lowThresh) ? ZERO : (data[pos] >= median) ? EDGE : ZERO;
}
