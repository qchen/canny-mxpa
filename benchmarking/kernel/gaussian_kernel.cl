__constant float gaus[3][3] __attribute__((section(".dram0.data"))) = {
  {0.0625, 0.125, 0.0625},
  {0.1250, 0.250, 0.1250},
  {0.0625, 0.125, 0.0625}
};
__kernel void gaussian_kernel(__global uchar16 *data,
                              __global uchar16 *out,
                              unsigned rows,
                              unsigned cols) {
  float16 sum = (float16)(0);
  unsigned row = get_global_id(0);
  unsigned col = get_global_id(1);
  cols = cols / 16;
  const unsigned POS = col       * cols + row;

  const unsigned N   = (col - 1) * cols + row;
  const unsigned S   = (col + 1) * cols + row;

  const unsigned E   = col       * cols + (row + 1);
  const unsigned W   = col       * cols + (row - 1);

  const unsigned NE  = (col - 1) * cols + (row + 1);
  const unsigned SW  = (col + 1) * cols + (row - 1);

  const unsigned SE  = (col + 1) * cols + (row + 1);
  const unsigned NW  = (col - 1) * cols + (row - 1);


  float16 dataE = (float16) {
    data[POS].s1, data[POS].s2, data[POS].s3, data[POS].s4,
    data[POS].s5, data[POS].s6, data[POS].s7, data[POS].s8,
    data[POS].s9, data[POS].sa, data[POS].sb, data[POS].sc,
    data[POS].sd, data[POS].se, data[POS].sf, data[E].s0
  };
  float16 dataW = (float16) {
    data[W].sf,   data[POS].s0, data[POS].s1, data[POS].s2,
    data[POS].s3, data[POS].s4, data[POS].s5, data[POS].s6,
    data[POS].s7, data[POS].s8, data[POS].s9, data[POS].sa,
    data[POS].sb, data[POS].sc, data[POS].sd, data[POS].se
  };
  float16 dataSE = (float16) {
    data[S].s1, data[S].s2, data[S].s3, data[S].s4,
    data[S].s5, data[S].s6, data[S].s7, data[S].s8,
    data[S].s9, data[S].sa, data[S].sb, data[S].sc,
    data[S].sd, data[S].se, data[S].sf, data[SE].s0
  };
  float16 dataSW = (float16) {
    data[SW].sf, data[S].s0, data[S].s1, data[S].s2,
    data[S].s3,  data[S].s4, data[S].s5, data[S].s6,
    data[S].s7,  data[S].s8, data[S].s9, data[S].sa,
    data[S].sb,  data[S].sc, data[S].sd, data[S].se
  };
  float16 dataN = convert_float16(data[N]);
  float16 dataS = convert_float16(data[S]);
  float16 dataNE = (float16) {
    data[N].s1, data[N].s2, data[N].s3, data[N].s4,
    data[N].s5, data[N].s6, data[N].s7, data[N].s8,
    data[N].s9, data[N].sa, data[N].sb, data[N].sc,
    data[N].sd, data[N].se, data[N].sf, data[NE].s0
  };
  float16 dataNW = (float16) {
    data[NW].sf, data[N].s0, data[N].s1, data[N].s2,
    data[N].s3,  data[N].s4, data[N].s5, data[N].s6,
    data[N].s7,  data[N].s8, data[N].s9, data[N].sa,
    data[N].sb,  data[N].sc, data[N].sd, data[N].se
  };

  float16 l_data[3][3] = {{dataNW, dataN, dataNE}, {dataW, convert_float16(data[POS]), dataE}, {dataSW, dataS, dataSE}};


  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      sum += gaus[i][j] * l_data[i][j];
  out[POS] = convert_uchar16(min(255, max(0, sum)));
}
