// Some of the available convolution kernels
__constant float sobx[3][3] __attribute__((section(".dram0.data"))) = {
  { -1, 0, 1},
  { -2, 0, 2},
  { -1, 0, 1}
};

__constant float soby[3][3] __attribute__((section(".dram0.data"))) = {
  { -1, -2, -1},
  { 0, 0, 0},
  { 1, 2, 1}
};

// Sobel kernel. Apply sobx and soby separately, then find the sqrt of their
//               squares.
// data:  image input data with each pixel taking up 1 byte (8Bit 1Channel)
// out:   image output data (8B1C)
// theta: angle output data
__kernel void sobel_kernel(__global uchar16 *data,
                           __global uchar16 *out,
                           __global uchar16 *theta,
                           unsigned rows,
                           unsigned cols) {
  // collect sums separately. we're storing them into floats because that
  // is what hypot and atan2 will expect.
  const float PI = 3.14159265;
  unsigned row = get_global_id(0);
  unsigned col = get_global_id(1);

  float16 angle = (float16)(0);
  float16 sumx = (float16)(0);
  float16 sumy = (float16)(0);

  cols = cols / 16;
  const unsigned POS = col       * cols + row;

  const unsigned N   = (col - 1) * cols + row;
  const unsigned S   = (col + 1) * cols + row;

  const unsigned E   = col       * cols + (row + 1);
  const unsigned W   = col       * cols + (row - 1);

  const unsigned NE  = (col - 1) * cols + (row + 1);
  const unsigned SW  = (col + 1) * cols + (row - 1);

  const unsigned SE  = (col + 1) * cols + (row + 1);
  const unsigned NW  = (col - 1) * cols + (row - 1);


  float16 dataE = (float16) {
    data[POS].s1, data[POS].s2, data[POS].s3, data[POS].s4,
    data[POS].s5, data[POS].s6, data[POS].s7, data[POS].s8,
    data[POS].s9, data[POS].sa, data[POS].sb, data[POS].sc,
    data[POS].sd, data[POS].se, data[POS].sf, data[E].s0
  };
  float16 dataW = (float16) {
    data[W].sf,   data[POS].s0, data[POS].s1, data[POS].s2,
    data[POS].s3, data[POS].s4, data[POS].s5, data[POS].s6,
    data[POS].s7, data[POS].s8, data[POS].s9, data[POS].sa,
    data[POS].sb, data[POS].sc, data[POS].sd, data[POS].se
  };
  float16 dataSE = (float16) {
    data[S].s1, data[S].s2, data[S].s3, data[S].s4,
    data[S].s5, data[S].s6, data[S].s7, data[S].s8,
    data[S].s9, data[S].sa, data[S].sb, data[S].sc,
    data[S].sd, data[S].se, data[S].sf, data[SE].s0
  };
  float16 dataSW = (float16) {
    data[SW].sf, data[S].s0, data[S].s1, data[S].s2,
    data[S].s3,  data[S].s4, data[S].s5, data[S].s6,
    data[S].s7,  data[S].s8, data[S].s9, data[S].sa,
    data[S].sb,  data[S].sc, data[S].sd, data[S].se
  };
  float16 dataN = convert_float16(data[N]);
  float16 dataS = convert_float16(data[S]);
  float16 dataNE = (float16) {
    data[N].s1, data[N].s2, data[N].s3, data[N].s4,
    data[N].s5, data[N].s6, data[N].s7, data[N].s8,
    data[N].s9, data[N].sa, data[N].sb, data[N].sc,
    data[N].sd, data[N].se, data[N].sf, data[NE].s0
  };
  float16 dataNW = (float16) {
    data[NW].sf, data[N].s0, data[N].s1, data[N].s2,
    data[N].s3,  data[N].s4, data[N].s5, data[N].s6,
    data[N].s7,  data[N].s8, data[N].s9, data[N].sa,
    data[N].sb,  data[N].sc, data[N].sd, data[N].se
  };

  float16 l_data[3][3] = {{dataNW, dataN, dataNE},
                          {dataW, convert_float16(data[POS]), dataE},
                          {dataSW, dataS, dataSE}};

  // find x and y derivatives
  for (int i = 0; i < 3; i++) {
    for (int j = 0; j < 3; j++) {
      sumx += sobx[i][j] * l_data[i][j];
      sumy += soby[i][j] * l_data[i][j];
    }
  }

  // The output is now the square root of their squares, but they are
  // constrained to 0 <= value <= 255. Note that hypot is a built in function
  // defined as: hypot(x,y) = sqrt(x*x, y*y).

  float16 sq = sqrt(sumx * sumx + sumy * sumy);
  uchar16 _out = convert_uchar16(min(255, max(0, convert_int16(sq))));
  out[POS] = _out;

  // Compute the direction angle theta in radians
  // atan2 has a range of (-PI, PI) degrees
  angle = atan2(sumy, sumx);

  // If the angle is negative,
  // shift the range to (0, 2PI) by adding 2PI to the angle,
  // then perform modulo operation of 2PI
  angle = angle < 0 ? fmod(angle + 2 * PI, 2 * PI) : angle;

  // Round the angle to one of four possibilities: 0, 45, 90, 135 degrees
  // then store it in the theta buffer at the proper position
  float16 tmp = (angle <= (float16)(1 * PI / 8)) ? (float16)(0) :
               (angle <= (float16)(3 * PI / 8)) ? (float16)(45) :
               (angle <= (float16)(5 * PI / 8)) ? (float16)(90) :
               (angle <= (float16)(7 * PI / 8)) ? (float16)(135) :
               (angle <= (float16)(9 * PI / 8)) ? (float16)(0) :
               (angle <= (float16)(11 * PI / 8)) ? (float16)(45) :
               (angle <= (float16)(13 * PI / 8)) ? (float16)(90) :
               (angle <= (float16)(15 * PI / 8)) ? (float16)(135) : (float16)(0);
  theta[POS] = convert_uchar16(tmp);

}
