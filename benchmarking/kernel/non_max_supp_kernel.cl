// Non-maximum Supression Kernel
// data: image input data with each pixel taking up 1 byte (8Bit 1Channel)
// out: image output data (8B1C)
// theta: angle input data
__kernel void non_max_supp_kernel(__global uchar16 *data,
                                  __global uchar16 *out,
                                  __global uchar16 *theta,
                                  unsigned rows,
                                  unsigned cols) {
  // These variables are offset by one to avoid seg. fault errors
  // As such, this kernel ignores the outside ring of pixels
  uchar16 ZERO = (uchar16)(0);
  unsigned row = get_global_id(0);
  unsigned col = get_global_id(1);

  cols = cols / 16;
  const unsigned POS = col       * cols + row;

  const unsigned N   = (col - 1) * cols + row;
  const unsigned S   = (col + 1) * cols + row;

  const unsigned E   = col       * cols + (row + 1);
  const unsigned W   = col       * cols + (row - 1);

  const unsigned NE  = (col - 1) * cols + (row + 1);
  const unsigned SW  = (col + 1) * cols + (row - 1);

  const unsigned SE  = (col + 1) * cols + (row + 1);
  const unsigned NW  = (col - 1) * cols + (row - 1);


  uchar16 dataE = (uchar16) {
    data[POS].s1, data[POS].s2, data[POS].s3, data[POS].s4,
    data[POS].s5, data[POS].s6, data[POS].s7, data[POS].s8,
    data[POS].s9, data[POS].sa, data[POS].sb, data[POS].sc,
    data[POS].sd, data[POS].se, data[POS].sf, data[E].s0
  };
  uchar16 dataW = (uchar16) {
    data[W].sf,   data[POS].s0, data[POS].s1, data[POS].s2,
    data[POS].s3, data[POS].s4, data[POS].s5, data[POS].s6,
    data[POS].s7, data[POS].s8, data[POS].s9, data[POS].sa,
    data[POS].sb, data[POS].sc, data[POS].sd, data[POS].se
  };
  uchar16 dataSE = (uchar16) {
    data[S].s1, data[S].s2, data[S].s3, data[S].s4,
    data[S].s5, data[S].s6, data[S].s7, data[S].s8,
    data[S].s9, data[S].sa, data[S].sb, data[S].sc,
    data[S].sd, data[S].se, data[S].sf, data[SE].s0
  };
  uchar16 dataSW = (uchar16) {
    data[SW].sf, data[S].s0, data[S].s1, data[S].s2,
    data[S].s3,  data[S].s4, data[S].s5, data[S].s6,
    data[S].s7,  data[S].s8, data[S].s9, data[S].sa,
    data[S].sb,  data[S].sc, data[S].sd, data[S].se
  };
  uchar16 dataN = data[N];
  uchar16 dataS = data[S];
  uchar16 dataNE = (uchar16) {
    data[N].s1, data[N].s2, data[N].s3, data[N].s4,
    data[N].s5, data[N].s6, data[N].s7, data[N].s8,
    data[N].s9, data[N].sa, data[N].sb, data[N].sc,
    data[N].sd, data[N].se, data[N].sf, data[NE].s0
  };
  uchar16 dataNW = (uchar16) {
    data[NW].sf, data[N].s0, data[N].s1, data[N].s2,
    data[N].s3,  data[N].s4, data[N].s5, data[N].s6,
    data[N].s7,  data[N].s8, data[N].s9, data[N].sa,
    data[N].sb,  data[N].sc, data[N].sd, data[N].se
  };

  out[POS] = (theta[POS] == ZERO) ? 
    (data[POS] <= dataE || data[POS] <= dataW) ? ZERO : data[POS] :
  (theta[POS] == (uchar16)(45)) ?
    (data[POS] <= dataNE || data[POS] <= dataSW) ? ZERO : data[POS] :
  (theta[POS] == (uchar16)(90)) ?
    (data[POS] <= dataN || data[POS] <= dataS) ? ZERO : data[POS]:
  (theta[POS] == (uchar16)(135)) ?
    (data[POS] <= dataNW || data[POS] <= dataSE) ? ZERO : data[POS] : data[POS];
}
